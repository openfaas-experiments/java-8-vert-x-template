# OpenFaaS Java 8 Vert.x template

This template provides additional context and control over the HTTP response from your function.

## Status of the template

This template is pre-release and is likely to change

## Supported platforms

* x86_64 - `java8-vert-x`

## Trying the template

```
$ faas template pull https://gitlab.com/openfaas-experiments/java-8-vert-x-template
$ faas new hello-vert-x --lang java8-vert-x
```

> **Remark**: where `hello-vert-x` is the name of your function

## Example usage

> WIP: 🚧